<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
define( 'DB_NAME', 'zoozle' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '123' );
define( 'DB_HOST', '127.0.0.1' );
define( 'DB_HOST_SLAVE', '127.0.0.1' );
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp');
define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME']);
define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content');
define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');

define('AUTH_KEY',         '1j*iAj#3@cES5xR+SP~$)]|d4X+^J[HHq!&:UH@o)2h>2dw6Be`JIW<UB/R($t7Q');
define('SECURE_AUTH_KEY',  'd*|c8~;r~R14)6{:V6b$x&o>iTMe75#>^ a>qjWhbuI`>$f_/HF,TiMfEWMV=&AA');
define('LOGGED_IN_KEY',    'S`Ph+3ZJ<tR!k38u[0/0@A7<UfN]1kQ%tJ2(z(2{ht.#U- $z#PviHg):^V2Pv#4');
define('NONCE_KEY',        'K+31m{P;mRM/QDca8{,xW+C2@y)]B0C][_aZy*$<q20c`Dc];6&@Qou4Z-MF8B*R');
define('AUTH_SALT',        'wA3YpWZuTPy,0D(r+vZ{0z=4J3=7PyohmOUYci*,&iL|{l0 +XARCY<F~/(*tF?0');
define('SECURE_AUTH_SALT', 'k-L_Qj~r036<46k0hPy/<HP|]6f#bn+>*dVFOy#u_XdUZLiE{6e-IS?UVP5[6O(`');
define('LOGGED_IN_SALT',   'KZYi&&,i:@_S1R]>z`Es7Z%3M*qi#RoPsyJ6_Y&)LPXEC)Nx=d$|!qIkAUL2;!g]');
define('NONCE_SALT',       'XVZ-&}+ ^TxmzazbD+qd4HQ 3+5 xG[I+2!B9LW+p3/xJ50O>#X 0-j0{Yq#-XJ+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');