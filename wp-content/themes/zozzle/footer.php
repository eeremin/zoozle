			

            </div> <!-- end #container -->

            

        

            <footer id="contact-section" class="footer">

            <div class="parallax-window color-overlay" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri();?>/images/footer_bg.png">

                <div class="clearfix">

                    <div class="container">

                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-6">

                                <div class="footer__logo">

                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/zozzle_logo.png" />

                                </div>

                                <div class="footer__socials">

                                    <a href="http://www.pinterest.com/zozzlemartinis" target="blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icn/Icons-01.png" /></a>

                                    <a href="http://fb.me/zozzlemartinis"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icn/Icons-02.png" /></a>

                                    <a href="https://twitter.com/zozzlemartinis"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icn/Icons-03.png" /></a>

                                    <a href="https://www.instagram.com/zozzlemartinis/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icn/Icons-04.png" /></a>

                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">

                                <div class="footer__contacts">

                                    <h4>Contact Us</h4>

                                    <p>Zozzle Martinis<br/>

                                    Buffalo NY<br/>

                                    <a href="tel:1-844-496-9953">1-844-4ZOZZLE</a></p>

                                </div>

                            </div>

                        </div>

                        <div class="footer__copyright">

                            <span><a href="<?= get_permalink(get_page_by_title('Terms Of Service')->ID) ?>">Terms of Service</a>. &#169;2016 Zozzlemartinis. All Rights Reserved. <a href="mailto:hello@zozzlemartinis.com">hello@zozzlemartinis.com</a></span>

                        </div>

                    </div>

                </div>

                </div>

			</footer> <!-- end footer -->

        

		

		

				

		<!--[if lt IE 7 ]>

  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>

  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>

		<![endif]-->

		

		<?php wp_footer(); // js scripts are inserted using this function ?>



        <script src="http://code.jquery.com/jquery.min.js"></script>

        <script src="/wp-content/themes/zozzle/js/parallax.js-1.4.2/parallax.min.js"></script>

        <script src="/wp-content/themes/zozzle/sweetalert/dist/sweetalert.min.js"></script>

        <script src="/wp-content/themes/zozzle/js/bootstrap-select.min.js"></script>

        <script src="/wp-content/themes/zozzle/js/script.js"></script>

        <script type="text/javascript">

            $('p').removeClass('lead');

            function addToCart(id, cart) {

                $.ajax("?add-to-cart=" + id, {

                    success: function() {

                        swal({

                            title:              "Product added to cart",

                            text:               "You've added a product to your cart!",

                            type:               "success",

                            showCancelButton:   true,

                            cancelButtonText:   "Continue Shopping",

                            confirmButtonText:  "Go To Cart",

                            closeOnConfirm:     true,

                            closeOnCancel:      true

                        }, function(isConfirm) {

                            if (isConfirm) {

                                window.location = cart;

                            }

                        })

                    }

                });

                console.log("Added product " + id + " to the cart");

            }





            $(function() {

              $("#header-links a[href*='#']:not([href='#'])").click(function() {

                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

                    var target = $(this.hash);

                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

                    if (target.length) {

                        $('html,body').animate({

                            scrollTop: target.offset().top

                        }, 500);

                        return false;

                    }

                }

              });

            });

        </script>

	</body>



</html>

