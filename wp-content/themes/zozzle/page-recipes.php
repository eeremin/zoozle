<?php get_header(); ?>
<?php
    $categorySlug = 'recipe';
    $cat = get_category_by_slug($categorySlug);
    $args = array('child_of' => $cat->cat_ID);
    $categories = get_categories( $args );
    $tags = get_terms(array('taxonomy' => 'post_tag', 'post_type' => 'post'));
?>
<div class="container">
    <div id="content" class="clearfix row">
        <div id="main" class="col-sm-12 clearfix posts-page" role="main">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="recipes clearfix">
                <div class="products__header clearfix">
                    <div class="products__categories">
                        <select class="selectpicker" name='post_cats'>
                            <option>All</option>
                            <?php foreach ($categories as $category): ?>
                                <option value="<?php echo $category->slug; ?>"><?php echo $category->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="products__tags clearfix"  data-attr="tag">
                        <?php foreach ($tags as $tag): ?>
                            <a class="product__tag" data-value="<?php echo $tag->slug; ?>" data-filter=".<?= $tag->slug; ?>"><?php echo $tag->name ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div id="post-ajax-list">
                    <?php   the_content(); ?>
                </div> <!-- end #post-ajax-list -->

                <?php if (function_exists('wp_bootstrap_page_navi')) : // if expirimental feature is active ?>

                    <?php wp_bootstrap_page_navi(); // use the page navi function ?>

                <?php  else : // if it is disabled, display regular wp prev & next links ?>
                    <nav class="wp-prev-next">
                        <ul class="pager">
                            <li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
                            <li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
                        </ul>
                    </nav>
                <?php endif; ?>
            </div>

        <?php endwhile; ?>

        <?php else : ?>
            <article id="post-not-found">
                <header>
                    <h1><?php _e("No recipes found.", "wpbootstrap"); ?></h1>
                </header>
                <section class="post_content">
                    <p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
                </section>
                <footer>
                </footer>
            </article>
        <?php endif; ?>
    </div>
</div>


<script>
    ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
    function refresh_page() {
        // get filter options
        var cat = $("[name='post_cats']").val();
        var tags = Array();
        $('[data-attr="tag"] a.match').each(function(key, value){
            tags.push($(value).attr('data-value'));
        });
        tags.join(',');

        jQuery.post(
            ajax_url,
            {
                'action': 'filter_post',
                'data':   {'cat':cat, 'tags':tags, 'category_slug': '<?php echo $categorySlug; ?>'}
            },
            function(response){
                //console.log('The server responded: ' + response);
                $('#post-ajax-list').html(response);
            }
        );
    }
    function change_page(page) {
        // get filter options
        var cat = $("[name='post_cats']").val();
        var tags = Array();
        $('[data-attr="tag"] a.match').each(function(key, value){
            tags.push($(value).attr('data-value'));
        });
        tags.join(',');

        jQuery.post(
            ajax_url,
            {
                'action': 'filter_post',
                'data':   {'cat':cat, 'tags':tags, 'page':page, 'category_slug': '<?php echo $categorySlug; ?>'}
            },
            function(response){
                //console.log('The server responded: ' + response);
                $('#post-ajax-list').html(response);
            }
        );
    }
    jQuery('body').on('click', '#post-ajax-list [data-page]', function(e){
        e.preventDefault();
        change_page(jQuery(this).attr('data-page'));
        return false;
    });

    (function($){
        $(document).ready(function ($) {
            var $container = $('#recipes-list');

            // get the first item to use for measuring columnWidth
            var $item = $container.find('.product').eq(0);

            // filter items when filter link is clicked
            $(document).on('click', '.products__tags a', function(e) {
                var dataKey = $(this).attr('data-key');
                if ($(this).hasClass('active')) {
                    var selector = false;
                    $(this).removeClass('active');

                } else {
                    var selector = $(this).attr('data-filter');
                    $(this).addClass('active');
                }
                $('.products__tags a.active').each(function(index, item) {
                    if (selector == false) {
                        selector = $(item).attr('data-filter');
                    } else {
                        if ($(item).attr('data-key') !== dataKey) {
                            selector += ', ' + $(item).attr('data-filter');
                        }
                    }
                });
                if (selector == false) {
                    selector = '*';
                }

                $container.isotope({ filter: selector, animationEngine : "css" });

                e.preventDefault();
            });

            $('[data-attr="tag"]').click(function () {
//                refresh_page();
            });

            $('[name="product_cats"]').change(function () {
                refresh_page();
            });

            $('.add_to_cart_button').on('click', function(e) {
                var quantity = $(this).parents().find('.product__order-value').val();
                $(this).attr('data-quantity', quantity);
            });
        });

    } ) ( jQuery );



</script>


<?php get_footer(); ?>
