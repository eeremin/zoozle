<?php

/*

Template Name: Homepage

*/

?>



<?php get_header(); ?>

    <div id="content" class="clearfix row home-page" data-spy="scroll" data-target="#header-links">

        <div id="main" class="clearfix" role="main">

            <div id="home" class="anchor"></div>

            <div class="landing parallax-window" id="landing"  data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri();?>/images/landing.png">

                <div class="container">

                 <!-- HEADING -->

                    <div class="home__heading">

                        <h2>Demystifying the Martini</h2>

                        <p>Zozzle keeps it simple and delicious, with style

                        by outfitting martini drinkers with no futz recipes 

                        and all the goods needed to whip up ab-so-lute-ly

                        delicious and fashionable cocktails at home.</p>

                    </div>

                </div>

            </div> 

            <div id="products-section" class="anchor"></div>

            <section class="products" id="products">

                <div class="color-overlay">

                    <div class="container">

                        <div id="products-mobile" style="padding-top: 15px; padding-bottom: 15px" class="row">

                            <div class="col-xs-6 product-intro">

                                <h3 class="main-title _dark _no-border">

                                    Martinis Are Tricky

                                </h3>

                                 <p>

                                    Zozzle keeps it simple and delicious, with style by

                                    outfitting martini drinkers with no futz recipes

                                    and all the goods needed to whip up ab-so-lute-

                                    ly delicious and fashionable cocktails at home.

                                </p>

                                <p>

                                    Dress up whatever you're shaking up with Zozzle's

                                    rim salts, sugars, and seasonings.

                                <p>

                                    Get in the spirit!

                                </p>

                            </div>

                            <!--    <div class="col-xs-10 col-xs-offset-1"><h2>FEATURED PRODUCTS</h2></div>-->

                            <div class="featured-products col-xs-6">

                                <div id="mobileCarousel" data-ride="carousel" class="carousel slide"  data-interval="10000">

                                    <div class="carousel-inner" role="listbox">

                                       <?php



                                        $products = get_woocommerce_featured_product_list();

                                        $count = 0;

                                        $active = true;

                                        foreach ($products as $product) {

                                            $active_class = $active? ' active ': '';

                                            $active = false;

                                            //never show more than 4

                                            if( ++$count > 4 ) { break; }

?>

                                                <div class="item <?php echo $active_class;?>"><a href="<?php echo get_permalink($product['id']) ?>">



    <?php

                                                    if (has_post_thumbnail( $product['id'] ) ) {

                                                        $image = wp_get_attachment_image_src(

                                                            get_post_thumbnail_id(

                                                                $product['id'] ),

                                                                'single-post-thumbnail'

                                                        );

    ?>

                                                        <img style="width: 100%; height: auto;" src="<?php echo $image[0]; ?>" />



    <?php

                                                    }

    ?>

                                                    <h3 class="featured-product-title small-caps"><?= $product['title'] ?></h3>

                                                    </a></div>



    <?php

                                                }

    ?>

                                            <a class="left carousel-control" href="#mobileCarousel" role="button" data-slide="prev">

                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

                                                <span class="sr-only">Previous</span>

                                            </a>

                                            <a class="right carousel-control" href="#mobileCarousel" role="button" data-slide="next">

                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

                                                <span class="sr-only">Next</span>

                                            </a> 

                                    </div>

                                </div>

                            </div>

                        </div>

                            <div style="margin-left: -15px; margin-right: -15px;" id="products-carousel" class="carousel slide" data-ride="carousel" data-interval="10000">

                            <div class="carousel-inner" role="listbox">

                                <div class="item active row">

                                    <div class="col-sm-offset-1 col-sm-5">

                                        <h3 class="main-title _dark _no-border">

                                            Martinis Are Tricky

                                        </h3>

                                        <p style="text-light">

                                            Zozzle keeps it simple and delicious, with style by 

                                            outfitting martini drinkers with no futz recipes 

                                            and all the goods needed to whip up ab-so-lute-

                                            ly delicious and fashionable cocktails at home.

                                        </p>

                                        <p style="text-light">

                                            Dress up whatever you.re shaking up with Zozzle.

                                            s rim salts, sugars, and seasonings. 

                                        <p style="text-light">

                                            Get in the spirit!

                                        </p>

                                    </div>

                                    <div id="martiniglass" style="background-color: #F8F8F8; text-align: center; opacity: .8; box-shadow: 2px 2px 2px black;" class="col-sm-5">

                                        <img style="box-shadow: 0px 0px 0px;" class="featured-img" src='<?php echo get_stylesheet_directory_uri();?>/images/sample9-grey.jpeg' />

                                    </div>

                                </div>

                                <div class="item row">

<?php

                                            $products = get_woocommerce_featured_product_list();

                                            $count = 0;

                                            foreach ($products as $product) {

                                                //never show more than 4

                                                if( ++$count > 4 ) { break; }

?>

                                                    <a href="<?php echo get_permalink($product['id']) ?>">

                                                        <div style="text-align: center;" class="col-lg-3 col-md-4 col-sm-6 product_<?php echo $count; ?>  product">

                                                            <div>

<?php

                                                            if (has_post_thumbnail( $product['id'] ) ) {

                                                                $image = wp_get_attachment_image_src( get_post_thumbnail_id(  $product['id'] ), 'single-post-thumbnail' );

?>

                                                                    <!--<div class="product-img" style="background-image: url('<?php echo $image[0]; ?>')"></div>

                                                                   --><img class="featured-img" style="width: 100%; height: auto;" src="<?php echo $image[0]; ?>" /> 

<?php

                                                            }

?>

                                                            <h3 class="featured-product-title small-caps"><?= $product['title'] ?></h3>

                                                            </div>

                                                        </div>

                                                    </a>

                                                   

<?php

                                            }

?>









                                                                </div>

                                    <a class="left carousel-control" style="background-image: none;" href="#products-carousel" role="button" data-slide="prev">

                                        <span class="glyphicon glyphicon-chevron-left" style="margin-left: -25px;" aria-hidden="true"></span>

                                        <span class="sr-only">Previous</span>

                                      </a>

                                  <a class="right carousel-control" style="background-image: none;" href="#products-carousel" role="button" data-slide="next">

                                    <span class="glyphicon glyphicon-chevron-right" style="margin-right: -25px;" aria-hidden="true"></span>

                                    <span class="sr-only">Next</span>

                                  </a>



                           </div><!-- end carousel inner -->

                        </div><!-- end product carousel -->

                    </div>

                </div>

            </section>

        </div><!-- products section div -->

        <div id="recipes-section" class="anchor"></div> 

        <section id="recipes">

            <div class="color-overlay parallax-window" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri();?>/images/recipie.jpg">

                <div class="container">

                    <h3 class="main-title">Featured Recipes</h3>

<?php

						$numberOfRecipes = 5;

                        $recipes = getBlogPosts('recipe', $numberOfRecipes);				

?>		

						<div class="row">



<?php 

                            for($imageNumber = 0; $imageNumber < ($numberOfRecipes); ++$imageNumber){

                                if (!$recipes[$imageNumber]) break;

                                if ($imageNumber == 0) { 

?>

                                <div class="home__recipes clearfix">

                                    <div class="home__1st-recipe clearfix">

                                        <div class="home__1st-recipe-img">

                                            <img src="<?=$recipes[$imageNumber]['image']?>" />

                                        </div>

                                        <div class="home__1st-recipe-content">

                                            <h4 itemprop="headline"><?= $recipes[$imageNumber]['title'] ?></h4></a>

                                            <div class="content"><?= $recipes[$imageNumber]['content'] ?></div>

                                        </div>

                                    </div>

                                

<?php

                                    } else {

?>

                                    <div class="col-sm-3 col-xs-6">

                                        <div class="home__recipe"><a class="home__recipe-link-outer" href="<?= get_category_link(get_cat_ID('recipe')) ?>#<?= $recipes[$imageNumber]['id']?>">

                                            <div class="home__recipe-effect"></div>

                                            <img src="<?=$recipes[$imageNumber]['image']?>" />

                                            <p class="home__recipe-title"><?= $recipes[$imageNumber]['title'] ?></p>

                                            <div class="home__recipe-link">Learn more</div>

                                        </div></a>

                                    </div>

<?php 

                                    }

                                }

?>

                                </div>

                            <!--

							<div class="col-sm-offset-1 col-sm-5 recipe">

									

						        <div id="diy-carousel2" class="carousel slide" data-ride="carousel" data-interval="10000">

                                

                                <div class="carousel-inner" role="listbox">

<?php 

                                    for($imageNumber = 1; $imageNumber < ($numberOfRecipes + 1); ++$imageNumber){ 

?>

                                        <div class="item <?php echo $imageNumber == 1 ? 'active': ''; ?>">

											<div class="recipe-img" style="background-image: url('<?=$recipes[$imageNumber-1]['image']?>"></div>

											<p class="recipe-title"><?= $recipes[$imageNumber-1]['title'] ?></p>

											<?= $recipes[$imageNumber-1]['content'] ?>

                                        </div>

<?php 

                                    }

?>

                                                                <!-- Controls 

                                        <a class="left carousel-control" href="#diy-carousel2" role="button" data-slide="prev">

                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

                                            <span class="sr-only">Previous</span>

                                        </a>

                                        <a class="right carousel-control" href="#diy-carousel2" role="button" data-slide="next">

                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

                                            <span class="sr-only">Next</span>

                                        </a>

                                </div><!-- end carousel inner 

								</div>

								</div>

                            </div> 

?>-->

                    <div class="home__more-recipes">

                        <a href="<?= get_permalink( get_page_by_path( 'recipes' ) ) ?>">See More Recipes</a>

                    </div>

                </div><!-- end of container -->

            </div><!-- end of recipe parallax -->

        </section><!-- end of recipes -->

        <!-- DIY section -->

        <div id="diy-section" class="anchor"></div>

        <section class="diy" id="diy">

            <div class="color-overlay">

                <div class="home__doit container">

                    <div class="row">

                        <div class="home__doit-text col-lg-4 col-sm-4 col-xs-12">

                            <h3 style="margin-top: 40px;" class="main-title _dark">Whip it up!</h3>

                            <p>What you'll need:</p>

                            <ul>

                                <li>1 small plate with a ring of rim sugar</li>

                                <li>1 small plate with a damp paper towel</li>

                                <li>Martini Glasses</li>

                                <li>Martini Mix</li>

                            </ul>

                        </div>

                        <div class="col-lg-6 col-lg-offset-1 col-sm-7 col-xs-12">



                            <div class="home__doit-slider-wrapper">

                                <div id="diy-carousel" class="carousel slide" data-ride="carousel" data-interval="10000">

                                    <ol class="carousel-indicators">

    <?php 

                                        $numberOfDiyImages = 7;

                                        for($imageNumber = 0; $imageNumber < $numberOfDiyImages; ++$imageNumber){ 

    ?>

                                            <li data-target="#diy-carousel" data-slide-to="<?=$imageNumber?>" class="<?php echo $imageNumber == 0 ? 'active': ''; ?>"></li>

    <?php 

                                        }

    ?>

                                    </ol>

                                    <div class="carousel-inner" role="listbox">

    <?php 

                                        for($imageNumber = 1; $imageNumber < ($numberOfDiyImages + 1); ++$imageNumber){ 

    ?>

                                            <div class="item <?php echo $imageNumber == 1 ? 'active': ''; ?>">

                                                <div class="carousel-img">

                                                    <div class="diy-img" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/diy<?php echo $imageNumber ?>.jpg')"></div>

                                                </div>

                                            </div>

    <?php 

                                        }

    ?>

                                        <!-- Controls -->

                                        

                                    </div><!-- end carousel inner -->

                                </div>



                                <a class="left carousel-control" href="#diy-carousel" role="button" data-slide="prev">

                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

                                            <span class="sr-only">Previous</span>

                                        </a>

                                        <a class="right carousel-control" href="#diy-carousel" role="button" data-slide="next">

                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

                                            <span class="sr-only">Next</span>

                                        </a>



                            </div>

							

                        </div>

                    </div>

                </div><!-- end container -->

            </div><!-- end color overlay -->

        </section>

        <!-- ABOUT US SECTION -->

        <div id="aboutus-section" class="anchor"></div>

        <section class="aboutus" id="aboutus">

            <div class="color-overlay">

                <div class="container-fluid">

                    <div style="position: relative;" class="row">

                        <div style="min-height: 405px;" class="col-lg-7 col-lg-offset-3 col-md-offset-4 col-sm-offset-5">

                            <h3 class="main-title _no-border _meet">Meet Robyn</h3>

                            <p>

                                She's an entrepreneur looking to strike that perfect balance in life.

                                Aren't we all? Robyn believes that hard work should be rewarded with simple

                                indulgences and good old-fashioned fun! She loves a well-mixed martini at

                                the end of a long day.

                            <p>

                                After years of hosting many a martini party, Robyn noticed that her guests 

                                were both smitten and and intimidated by this classic cocktail. 

                                The problem, she realized, lied with the preparation, not the consumption. 

                                Thus, the idea for Zozzle was born.

                            </p>

                            <p>

                                Robyn is a martini lover on a mission to liberate us from drab cocktails 

                                and lackluster libations. 

                                She launched Zozzle to demystify the martini and outfit those that want 

                                more with all the goods they need to shake it up.

                            </p>

                        </div>

                        <div id="robyn-img"></div>

                    </div>

                </div>

            </div>

        </section>

        <!-- services section -->

        <div id="services-section" class="anchor"></div>

        <section class="services" id="services">

            <div class="color-overlay">

                <div class="container">            

                    <h3 class="main-title _dark">Our Services</h3>     

                    <div class="row">

                        <div class="col-md-6 col-xs-12">

                            <div class="services__service clearfix">

                                <!--<div class="services__service-img"><img src="<?php echo get_stylesheet_directory_uri();?>/images/sample3.jpg" /></div>-->

                                <div class="services__service-img" style="background-image: url(<?php echo get_stylesheet_directory_uri();?>/images/sample3.jpg);"></div>

                                <div class="services__service-content">

                                    <h4>Martinis for your Dinner Party or Event</h4>

                                    <p>

                                        Need Martini glasses to fit your style?<br/>
                                        We'll work with you to create the perfect glass!

                                    </p>

                                </div>

                                

                            </div>

                            <div class="services__link">
                                <a href="<?= get_permalink( get_page_by_path( 'martinis-for-your-event' ) ) ?>">See more</a>
                            </div>

                        </div>

                        <div class="col-md-6 col-xs-12">

                            <div class="services__service clearfix">

                                <!--<div class="services__service-img"><img src="<?php echo get_stylesheet_directory_uri();?>/images/sample5.jpg" /></div>-->

                                <div class="services__service-img" style="background-image: url(<?php echo get_stylesheet_directory_uri();?>/images/sample5.jpg);"></div>

                                <div class="services__service-content">

                                    <h4>Martini Menus for Bars & Restaurants   </h4>

                                    <p>

                                        Our professional staff will<br/>
                                        make your party come to<br/>
                                        life! We'll do all the work<br/>
                                        so you don't have to!                         

                                    </p>

                                </div>

                                

                            </div>

                            <div class="services__link">
                                    <a href="<?= get_permalink( get_page_by_path( 'zozzle-cocktail-program' ) ) ?>">See more</a>
                                </div>

                            

                        </div>

                    </div>

                </div>

            </div>

        </section><!-- end services -->

        <!-- contact section --

        <div id="contact-section" class="anchor"></div>

        <section class="contact" id="contact">

            <div class="parallax-window color-overlay" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri();?>/images/contact.png">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-sm-offset-1 col-sm-3" style="opacity: .8;">

                        <h3 style="margin-top: 40px;" class="small-caps">Contact Us</h3>

                        <p>

                            Zozzle LLC<br>

                            PO Box 373<br>

                            Buffalo NY 14240

                        </p>

                        <p style="margin-bottom: 40px;">Email: hello@zozzle.com</p>

                        <p>

                        <div class="text-center contact-media">

                            <a href="https://twitter.com/ZozzleMartinis"><div class="media-icon twitter"><i class="fa fa-twitter" aria-hidden="true"></i></div></a>

                        </div>

                        <div class="text-center contact-media">

                            <a href="http://www.pinterest.com/zozzlemartinis"><div class="media-icon pinterest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div></a>

                        </div>

                        <div class="text-center contact-media">

                            <a href="https://www.instagram.com/zozzlemartinis/"><div class="media-icon instagram"><i class="fa fa-instagram" aria-hidden="true"></i></div></a>

                        </div>

                        <div class="text-center contact-media">

                            <a href="http://fb.me/zozzlemartinis"><div class="media-icon facebook"><i class="fa fa-facebook" aria-hidden="true"></i></div></a>

                        </div>

                        </p>

                    </div>

                </div>

            </div>

        </section> <!-- end of contact section -->

    </div>

<?php get_footer(); ?>

