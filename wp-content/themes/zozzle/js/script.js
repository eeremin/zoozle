jQuery(document).ready(function($){

	$(document).on('click', '.product__order-btn-up', function(e) {

		$(e.target).siblings('input[type="number"]').val(function(i, oldval){

			if(oldval == 99) return 99;

				else return ++oldval;

		});

	});
	$(document).on('click', '.product__order-btn-down', function(e) {

		$(e.target).siblings('input[type="number"]').val(function(i, oldval){

			if(oldval == 1) return 1;

				else return --oldval;

		});

	});

	$('.post__content').each(function(i, elem) {
		if($(this).outerHeight() == 231) {
			$(this).siblings('.post__content-show-link').show();
		}
	});
	$(document).on("click", '.post__content-show-link', function(e) { 
		e.preventDefault();
		$(e.target).siblings('.post__content').addClass('_full');

	});
	$(document).on("click", '.post__content-hide-link', function(e) { 
		e.preventDefault();
		$(e.target).parent().removeClass('_full');
	});

	// woo single product btns

	$('.woocommerce div.product form.cart div.quantity input').wrap('<div class="woo__input-wrapper"></div>');

	$('.woo__input-wrapper').append('<div class="woo__input-up-btn"></div>');

	$('.woo__input-wrapper').append('<div class="woo__input-down-btn"></div>');

	//cart inputs 

	$('.woocommerce table.cart__main-table td.product-quantity .quantity').append('<div class="woo__input-up-btn"></div>');

	$('.woocommerce table.cart__main-table td.product-quantity .quantity').append('<div class="woo__input-down-btn"></div>');



	$(document).on('click', '.woo__input-up-btn', function(e){

		$(e.target).siblings('input[type="number"]').val(function(i, oldval){

			if(oldval == 99) return 99;

				else return ++oldval;

		});

	});

	$(document).on('click', '.woo__input-down-btn', function(e){

		$(e.target).siblings('input[type="number"]').val(function(i, oldval){

			if(oldval == 1) return 1;

				else return --oldval;

		});

	});

	//$('.product__tag').click(function(e){
    //
	//	if ($(this).hasClass("match"))
    //
	//		$(this).removeClass("match");
    //
	//	else
    //
	//		$(this).addClass("match");
    //
	//});





});