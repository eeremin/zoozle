<?php get_header(); ?>
			
			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix" role="main">

				<div class="container">

					<article id="post-not-found" class="clearfix not-found">
						
						<header>

							<div class="hero-unit">
							
								<h1><?php _e("Epic 404 - Article Not Found","wpbootstrap"); ?></h1>
								<p><?php _e("This is embarassing. We can't find what you were looking for.","wpbootstrap"); ?></p>
															
							</div>
													
						</header> <!-- end article header -->

						
						<footer>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->

					</div>
			
				</div> <!-- end #main -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>