<?php
// Handle request then generate response using WP_Ajax_Response
global $wpdb;
$post = $_POST['data'];
$tax_query = array();
$page = 1;
$per_page = 12;

if (isset($post['page']) && $post['page'] !== '') {
    $page = $post['page'];
}

$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => $per_page,
    'paged' => $page,
);

if (isset($post['tags']) && $post['tags'] !== '') {
    $tax_query['relation'] = 'AND';
    $tax_query[] = array(
        'taxonomy' => 'post_tag',
        'field' => 'slug',
        'terms' => $post['tags'],
        'operator' => 'IN',
    );
}

if (isset($post['cat']) && $post['cat'] !== '' && $post['cat'] !== 'All') {
    $tax_query[] = array(
        'taxonomy' => 'category',
        'field' => 'slug',
        'terms' => $post['cat'],
    );
    $category = get_category_by_slug($post['cat']);
} else {
    if (isset($post['category_slug']) || isset($category->slug)) {
        $categorySlug = isset($post['category_slug']) ? $post['category_slug'] : $category->slug;
        $args['category_name'] = $categorySlug;
    }
}

if ($tax_query) {
    $args['tax_query'] = $tax_query;
}

$posts_array = new WP_Query($args);
?>
<?php if ($posts_array->have_posts()) : ?>
    <div class="row" id="recipes-list">
        <?php while ($posts_array->have_posts()) : $posts_array->the_post(); ?>
            <?php
            $posttags = get_the_tags();
            $taxonomy_classes = array();
            if ($posttags) {
               foreach ($posttags as $tag) {
                   $taxonomy_classes[] = $tag->slug;
               }
            }
            ?>
            <div class="col-lg-4 col-sm-6 post-card <?= (!empty($taxonomy_classes)) ? implode(' ', $taxonomy_classes) : ''; ?>">
                <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix post__recipe post'); ?> role="article">

                    <header class="post__header">
                        <div class="post__img">
                            <img src="<?= wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>"/>
                            <h4 class="post__title" itemprop="headline"><?php the_title(); ?></h4>
                        </div>
                    </header> <!-- end article header -->

                    <section class="post__content-wrapper">
                        <div class="post__content">
                            <?php
                            the_content();
                            ?>
                            <a href="#" class="post__content-hide-link">Hide</a>
                        </div> <!-- end article section -->

                        <a href="#" class="post__content-show-link" style="display: inline">Show</a>
                    </section>

                    <footer class="post__tags clearfix">

                        <?php if ($posttags) : ?>
                            <?php foreach ($posttags as $posttag): ?>
                                <a href="#" class="product__tag"><?php echo $posttag->name; ?></a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </footer> <!-- end article footer -->

                </article> <!-- end article -->
            </div>

        <?php endwhile; ?>
    </div>
<?php else : ?>
    <article id="post-not-found">
        <header>
            <h1><?php _e(sprintf("No %s found.", $category->slug), "wpbootstrap"); ?></h1>
        </header>
    </article>
<?php endif; ?>
<?php
    wp_reset_postdata();
?>
<?php
$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'tax_query' => $tax_query,
);

$count_post = get_posts($args);
$count = ceil(count($count_post) / $per_page);
?>


<div class="products__pagination">
    <?php if ($page > 1 && $count > 1): ?>
        <div class="prev"><a href="" data-page="<?php echo $page - 1; ?>">prev</a></div>
    <?php endif; ?>
    <div class="pages">
        <?php if ($count > 1) : ?>
            <?php for ($i = 1; $i < $count + 1; $i++): ?>
                <a href="#"
                   data-page="<?php echo $i; ?>" <?php echo $i == $page ? ' class="active"' : ''; ?> ><?php echo $i; ?></a>
            <?php endfor; ?>
        <?php endif; ?>
    </div>
    <?php if ($page < $count && $count > 1): ?>
        <div class="next"><a href="" data-page="<?php echo $page + 1; ?>">next</a></div>
    <?php endif; ?>
</div>

</div><!-- end  #product-ajax-list -->