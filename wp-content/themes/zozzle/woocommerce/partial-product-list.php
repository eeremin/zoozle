<?php
// Handle request then generate response using WP_Ajax_Response
global $wpdb;
$post = $_POST['data'];
$tax_query = '';
$category = '';
$page = 1;
$per_page = 12;

if (isset($post['tags']) && $post['tags'] !== '') {
    $tax_query['relation'] = 'AND';
    $tax_query[] = array(
        'taxonomy' => 'product_tag',
        'field' => 'slug',
        'terms' => $post['tags'],
        'operator' => 'IN',
    );
}

if (isset($post['cat']) && $post['cat'] !== '' && $post['cat'] !== 'All') {
    $tax_query[] = array(
        'taxonomy' => 'product_cat',
        'field' => 'slug',
        'terms' => $post['cat'],
    );
}

if (isset($post['page']) && $post['page'] !== '') {
    $page = $post['page'];
}

$args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => $per_page,
    'tax_query' => $tax_query,
    'paged' => $page
);

$posts_array = new WP_Query($args);
$product_list = [];
while ($posts_array->have_posts()) : $posts_array->the_post();
    $theid = get_the_ID();
    $product = new WC_Product($theid);

    $attrs = get_post_meta(get_the_ID(), '_product_attributes', true);
    $attributes = array();
    foreach ($attrs as $attr) {
        $attributes[] = array(
            'name' => $attr['name'],
            'value' => $attr['value'],
        );
    }

    $product_list[] = array(
        'id' => $theid,
        'title' => get_the_title($theid),
        'price' => $product->price,
        'content' => get_post($theid)->post_content,
        'attributes' => $attributes,
        'product' => $product,
    );
endwhile;

wp_reset_postdata();

foreach ($product_list as $product):
    $product_price_dollars = substr($product['price'], 0, -3);
    $product_price_cents = substr($product['price'], -2);
    $cart = get_permalink(get_page_by_title('Cart')->ID);
    $prod_tags = get_the_terms($product['id'], 'product_tag');
    $taxonomy_classes = array();
    if (is_array($prod_tags)) {
        foreach ($prod_tags as $prod_tag) {
            $taxonomy_classes[] = $prod_tag->slug;
        }
    }
    ?>
    <div class="product product-fix col-xs-12 col-sm-6 col-md-4 <?= (!empty($taxonomy_classes)) ? implode(' ', $taxonomy_classes) : ''; ?>">
        <?php
        if (has_post_thumbnail($product['id'])) {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($product['id']), 'single-post-thumbnail');
            ?>
            <img src="<?php echo $image[0]; ?>"/>
            <?php
        }
        ?>
        <div class="product__description clearfix">
            <h3><a href="<?= get_permalink($product['product']->post->ID) ?>"><?= $product['title'] ?></a></h3>

            <p><?= $product['content'] ?></p>

            <div class="product__order clearfix">
                <div class="product__price">
                    <div class="product__price-dollars">$<?= $product_price_dollars ?></div>
                    <div class="product__price-cents"><?= $product_price_cents ?></div>
                </div>
                <div class="product__quantity clearfix">
                    <input type="number" value="1" class="product__order-value"/>
                    <button class="product__order-btn-up"></button>
                    <button class="product__order-btn-down"></button>
                </div>
                <button class="add-to-cart" onclick="addToCart(<?= $product['id'] ?>, '<?= $cart ?>')">Add to Cart
                </button>
            </div>

            <div class="product__tags clearfix">
                <?php $prod_tags = get_the_terms($product['id'], 'product_tag');
                if ($prod_tags) {
                    foreach ($prod_tags as $prod_tag) {
                        ?>
                        <a class="product__tag"><?php echo $prod_tag->name; ?></a>
                    <?php }
                } ?>
            </div>

        </div>
    </div>
<?php endforeach; ?>


<?php

$args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'tax_query' => $tax_query,
);


$count_post = get_posts($args);
$count = ceil(count($count_post) / $per_page);
?>