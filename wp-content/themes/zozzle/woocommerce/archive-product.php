<?php get_header();
$args = array(
    'number' => 0,
    'orderby' => "name",
    'order' => "ASC",
    'hide_empty' => 1

);
$product_categories = get_terms('product_cat', $args);
$product_tags = get_terms('product_tag', $args);
?>


    <div id="content" class="spice-single clearfix">
        <div id="main" class="clearfix products-page" role="main">
            <div class="container" data-filter="products">
                <?php wc_print_notices(); ?>
                <div class="products__header clearfix">
                    <div class="products__categories">
                        <select name="product_cats" class="selectpicker">
                            <option>All</option>
                            <?php foreach ($product_categories as $product_cat) { ?>
                                <option value="<?php echo $product_cat->slug; ?>"><?php echo $product_cat->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="products__tags clearfix" data-attr="tag">
                        <?php $tags_count = 0; ?>
                        <?php foreach ($product_tags as $key => $product_tag) { ?>
                            <a class="product__tag" id="tag-<?= $key; ?>" data-key="<?= $key; ?>" data-filter=".<?= $product_tag->slug; ?>"
                               data-value="<?= $product_tag->slug; ?>"><?php echo $product_tag->name; ?></a>
                            <?php if (++$tags_count > 4) break; ?>
                        <?php } ?>
                    </div>
                </div>
                <div>
                    <div class="row" id="product-ajax-list">
                    <?php
                        $per_page = 12;
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $args = array(
                            'post_type' => 'product',
                            'post_status' => 'publish',
                            'posts_per_page' => $per_page,
                            'paged' => $paged
                        );
                        $productLists = get_woocommerce_product_list($args);
                        foreach ($productLists as $productList) {
                            $product = $productList['product'];
                            $product_price_dollars = substr($productList['price'], 0, -3);
                            $product_price_cents = substr($productList['price'], -2);
                            $cart = get_permalink(get_page_by_title('Cart')->ID);
                            $prod_tags = get_the_terms($productList['id'], 'product_tag');
                            $taxonomy_classes = array();
                            if (is_array($prod_tags)) {
                                foreach ($prod_tags as $prod_tag) {
                                    $taxonomy_classes[] = $prod_tag->slug;
                                }
                            }
                            ?>
                            <div class="product product-fix col-xs-12 col-sm-6 col-md-4 <?= (!empty($taxonomy_classes)) ? implode(' ', $taxonomy_classes) : ''; ?>">
                                <?php
                                if (has_post_thumbnail($productList['id'])) {
                                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($productList['id']), 'single-post-thumbnail');
                                    ?>
                                    <img src="<?php echo $image[0]; ?>"/>
                                    <?php
                                }
                                ?>
                                <div class="product__description clearfix">
                                    <h3>
                                        <a href="<?= get_permalink($productList['product']->post->ID) ?>"><?= $productList['title'] ?></a>
                                    </h3>
                                    <p><?= $productList['content'] ?></p>
                                    <div class="product__order clearfix">
                                        <div class="product__price">
                                            <div class="product__price-dollars">$<?= $product_price_dollars ?></div>
                                            <div class="product__price-cents"><?= $product_price_cents ?></div>
                                        </div>
                                        <div class="product__quantity clearfix">
                                            <input type="number" value="1" class="product__order-value"/>
                                            <button class="product__order-btn-up"></button>
                                            <button class="product__order-btn-down"></button>
                                        </div>
                                        <!--<button class="add-to-cart" onclick="addToCart(<?= $productList['id'] ?>, '<?= $cart ?>')">Add to Cart</button>-->
                                        <a href="<?php echo esc_url($product->add_to_cart_url()); ?>" data-quantity="1"
                                           data-product_sku="<?php echo esc_attr($product->get_sku()); ?>"
                                           class="product_type_simple add_to_cart_button add-to-cart">
                                            <?php _e('Add to cart'); ?>
                                        </a>
                                    </div>
                                    <div class="product__tags clearfix">
                                        <?php
                                        if ($prod_tags) {
                                            foreach ($prod_tags as $prod_tag) {
                                                ?>
                                                <a class="product__tag" href="<?php echo get_tag_link($prod_tag->term_id); ?>"><?php echo $prod_tag->name; ?></a>
                                            <?php }
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <?php /* woocommerce_content(); */ ?>
                    </div>
                    <?php
                        $productLists = get_woocommerce_product_list();
                        $count = ceil(count($productLists)/$per_page);
                    ?>
                    <div class="products__pagination">
                        <?php previous_posts_link(); ?>
                        <?php next_posts_link(); ?>
                        <?php the_posts_pagination(); ?>
                        <?php if ($paged > 1 && $count > 1): ?>
                            <div class="prev"><a href="<?php echo get_pagenum_link($paged + 1); ?>" data-page="<?php echo $paged - 1; ?>" >prev</a></div>
                        <?php endif; ?>
                        <div class="pages">
                            <?php if ($count > 1) : ?>
                                <?php for ($i = 1; $i < $count + 1; $i++): ?>
                                    <a href="<?php echo get_pagenum_link($i); ?>" data-page="<?php echo $i; ?>" <?php echo $i == $paged?' class="active"':''; ?> ><?php echo $i; ?></a>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </div>
                        <?php if ($paged < $count && $count > 1): ?>
                            <div class="next"><a href="<?php echo get_pagenum_link($paged + 1); ?>" data-page="<?php echo $paged + 1; ?>" >next</a></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div><!-- end  #product-ajax-list -->
        </div> <!-- end #main -->
        <?php //get_sidebar(); // sidebar 1 ?>
    </div> <!-- end #content -->

    <script>
        ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
        function refresh_page() {
            // get filter options
            var cat = $("[name='product_cats']").val();
            var tags = Array();
            $('[data-attr="tag"] a.match').each(function (key, value) {
                tags.push($(value).attr('data-value'));
            });
            tags.join(',');

            jQuery.post(
                ajax_url,
                {
                    'action': 'filter_product',
                    'data': {'cat': cat, 'tags': tags}
                },
                function (response) {
                    //console.log('The server responded: ' + response);
                    $('#product-ajax-list').html(response);
                }
            );
        }
        function change_page(page) {
            // get filter options
            var cat = $("[name='product_cats']").val();
            var tags = Array();
            $('[data-attr="tag"] a.match').each(function (key, value) {
                tags.push($(value).attr('data-value'));
            });
            tags.join(',');

            jQuery.post(
                ajax_url,
                {
                    'action': 'filter_product',
                    'data': {'cat': cat, 'tags': tags, 'page': page}
                },
                function (response) {

                    $('#product-ajax-list').html(response);
                }
            );
        }
        jQuery('body').on('click', '#product-ajax-list [data-page]', function (e) {
            e.preventDefault();
            change_page(jQuery(this).attr('data-page'));
            return false;
        });

        (function($){
            $(document).ready(function ($) {
                var $container = $('#product-ajax-list');

                // create a clone that will be used for measuring container width
//                $containerProxy = $container.clone().empty().css({ visibility: 'hidden' });
//
//                $container.after( $containerProxy );

                // get the first item to use for measuring columnWidth
                var $item = $container.find('.product').eq(0);

                // filter items when filter link is clicked
                $(document).on('click', '.products__tags a', function(e) {
                    var dataKey = $(this).attr('data-key');
                    if ($(this).hasClass('active')) {
                        var selector = false;
                        $(this).removeClass('active');

                    } else {
                        var selector = $(this).attr('data-filter');
                        $(this).addClass('active');
                    }
                    $('.products__tags a.active').each(function(index, item) {
                        if (selector == false) {
                            selector = $(item).attr('data-filter');
                        } else {
                            if ($(item).attr('data-key') !== dataKey) {
                                selector += ', ' + $(item).attr('data-filter');
                            }
                        }
                    });
                    if (selector == false) {
                        selector = '*';
                    }

                    $container.isotope({ filter: selector, animationEngine : "css" });

                    e.preventDefault();
                });

                $('[data-attr="tag"]').click(function () {
//                refresh_page();
                });

                $('[name="product_cats"]').change(function () {
                    refresh_page();
                });

                $('.add_to_cart_button').on('click', function(e) {
                    var quantity = $(this).parents().find('.product__order-value').val();
                    $(this).attr('data-quantity', quantity);
                });
            });

        } ) ( jQuery );

    </script>


<?php get_footer(); ?>