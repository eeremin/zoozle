<?php get_header(); ?>

			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix posts-page" role="main" data-parallax="scroll" 
                    data-position="right top" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/images/blog.jpg">
                    
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="col-md-4 post-card">
                        <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
                            
                            <header>
                                
                                <div class="row">
                                    <h1 class="single-title" itemprop="headline"><?php the_title(); ?></h1>
                                </div>
                                
                                <div class="row">
                                    <p class="meta"><?php _e("Posted", "wpbootstrap"); ?> <time datetime="<?php echo the_time('Y-m-j'); ?>" pubdate><?php the_time('m/d/Y h:m a'); ?></time> <?php _e("by", "wpbootstrap"); ?> <?php the_author(); ?> <span class="amp">&</span> <?php _e("filed under", "wpbootstrap"); ?> <?php the_category(', '); ?>.</p>
                                </div>
                            
                            </header> <!-- end article header -->

                            <section class="post_content">
                                <div class="row">
                                    <div class="post-img" style="background-image: url('<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) ?>')"></div>
                                </div>
                                <div class="row">
                                    <?php the_content(); ?>
                                </div>
                        
                            </section> <!-- end article section -->
                            
                            <footer>
                                
                            </footer> <!-- end article footer -->
                        
                        </article> <!-- end article -->
                    </div>
                    
                    <?php endwhile; ?>	
                    
                    <?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
                        
                        <?php wp_bootstrap_page_navi(); // use the page navi function ?>

                    <?php } else { // if it is disabled, display regular wp prev & next links ?>
                        <nav class="wp-prev-next">
                            <ul class="pager">
                                <li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
                                <li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
                            </ul>
                        </nav>
                    <?php } ?>
                                
                    
                    <?php else : ?>
                    
                    <article id="post-not-found">
                        <header>
                            <h1><?php _e("No Posts Yet", "wpbootstrap"); ?></h1>
                        </header>
                        <section class="post_content">
                            <p><?php _e("Sorry, What you were looking for is not here.", "wpbootstrap"); ?></p>
                        </section>
                        <footer>
                        </footer>
                    </article>
                    
                    <?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
