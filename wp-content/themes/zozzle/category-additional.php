<?php get_header(); ?>

<div class="container">
    <div id="content" class="clearfix row">
        <div id="main" class="col-sm-12 clearfix posts-page" role="main">
            <h3 class="main-title"><?php  _e('Ideas'); ?></h3>
            <div class="recipes clearfix">
                <?php if (have_posts()) :
                    while (have_posts()) : the_post(); ?>
                        <div class="col-lg-4 col-sm-6 post-card">
                            <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix post__recipe'); ?> role="article">
                                <header class="post__header">
                                    <div class="post__img">
                                        <img src="<?= wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>"/>
                                        <h4 class="post__title" itemprop="headline"><?php the_title(); ?></h4>
                                    </div>
                                </header> <!-- end article header -->

                                <section class="post__content-wrapper">
                                    <div class="post__content">
                                        <h4 class="post__date"><?php echo get_the_date(); ?></h4>
                                        <?php
                                        the_content();
                                        ?>
                                        <a href="#" class="post__content-hide-link">Hide</a>
                                    </div> <!-- end article section -->
                                    <a href="#" class="post__content-show-link">Show</a>
                                </section>
                            </article> <!-- end article -->
                        </div>
                    <?php endwhile; ?>
                </div>

                <?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
                    <?php wp_bootstrap_page_navi(); // use the page navi function ?>
                <?php } else { // if it is disabled, display regular wp prev & next links ?>
                    <nav class="wp-prev-next">
                        <ul class="pager">
                            <li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
                            <li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
                        </ul>
                    </nav>
                <?php } ?>
            <?php else : ?>
                <article id="post-not-found">
                    <header>
                        <h1><?php _e("No Posts Yet", "wpbootstrap"); ?></h1>
                    </header>
                    <section class="post_content">
                        <p><?php _e("Sorry, What you were looking for is not here.", "wpbootstrap"); ?></p>
                    </section>
                    <footer>
                    </footer>
                </article>
            <?php endif; ?>
        </div> <!-- end #main -->
        <?php //get_sidebar(); // sidebar 1 ?>
    </div> <!-- end #content -->
</div>

<?php get_footer(); ?>
