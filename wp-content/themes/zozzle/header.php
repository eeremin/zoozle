<!doctype html>  



<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->

<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->

<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->

<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->

<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	

	<head>

		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title( '|', true, 'right' ); ?></title>	

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">



        <link rel="stylesheet" href="/wp-content/themes/zozzle/css/font-awesome-4.6.2/css/font-awesome.min.css">

        <link rel="stylesheet" href="/wp-content/themes/zozzle/sweetalert/dist/sweetalert.css">

        <link rel="stylesheet" href="/wp-content/themes/zozzle/css/bootstrap-select.min.css">

		<!-- wordpress head functions -->

		<?php wp_head(); ?>

		<!-- end of wordpress head -->

		<!-- IE8 fallback moved below head to work properly. Added respond as well. Tested to work. -->

			<!-- media-queries.js (fallback) -->

		<!--[if lt IE 9]>

			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			

		<![endif]-->



		<!-- html5.js -->

		<!--[if lt IE 9]>

			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

		<![endif]-->	

		

			<!-- respond.js -->

		<!--[if lt IE 9]>

		          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>

		<![endif]-->	

	</head>

	

<?php

        $background_class = 'purple-background';

        foreach (array('Cart', 'Checkout') as $page) {

            if (is_page($page)) {

                $background_class = 'white-background';

            }

        }



        if (is_front_page()) {

            $background_class = '';

        }

?>

	<body <?php body_class(); ?> data-spy="scroll" data-target="#header-links">

				

		<header role="banner">

				

			<div class="navbar navbar-inverse navbar-fixed-top">

				<div class="container">

          

					<div class="navbar-header">

                        <div class="navbar-brand">

                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            
                            	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/zozzle_logo.png" alt="Zerif">

                            </a>

                        </div>

						

						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-links">

							<span class="icon-bar"></span>

							<span class="icon-bar"></span>

							<span class="icon-bar"></span>

						</button>

					</div>



					<div id="header-links" class="collapse navbar-collapse navbar-responsive-collapse header">

                        <ul class="nav navbar-nav navbar-center pull-right responsive-nav main-nav-list">

<?php

                            $url_to_home = (is_front_page()) ? '': get_permalink(get_page_by_title('Home')->ID);
							$category_id = get_cat_ID( 'additional' );
							$category_link = get_category_link( $category_id );

?>

                            <li><a href="<?php echo  $url_to_home; ?>#home">The Spirit</a></li>

                            <li><a href="<?php echo  $url_to_home; ?>/shop">Products</a></li>

                            <!-- <li><a href="<?php echo  $url_to_home; ?>/category/blog"><h4>blog</h4></a></li> -->

                            <li><a href="<?php echo  $url_to_home; ?>#recipes-section">Recipes</a></li>

                            <li><a href="<?php echo  $url_to_home; ?>#diy-section">DIY</a></li>

                            <li><a href="<?php echo  $url_to_home; ?>#aboutus-section">About Us</a></li>

                            <li><a href="<?php echo  $url_to_home; ?>#services-section">Our Services</a></li>

                            <li><a href="<?php echo  $url_to_home; ?>#contact-section">Contact</a></li>

                            <li><a href="<?php echo  get_permalink(get_page_by_title('Cart')->ID); ?>">Cart <span style="color: #D31E13; font-weight: bold;">( <?php echo WC()->cart->get_cart_contents_count(); ?> )</span></a></li>

                        </ul>

                    </div>

				</div> <!-- end .container -->

			</div> <!-- end .navbar -->

		

		</header> <!-- end header -->

		

		<div id="body" class="container-fluid <?= $background_class ?>">

