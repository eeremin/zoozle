<?php
/*
Template Name: custom page
*/
?>

<?php get_header(); ?>

			<div id="content" class="clearfix row">

                <div class="container">

                    <div class="row">
    			
        				<div id="main" class="col-sm-12 clearfix posts-page" role="main">

                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div class="custom_page">
                                    <header>
                                        
                                        <h1 class="single-title" itemprop="headline"><?php the_title(); ?></h1>
                                        
                                    </header> <!-- end article header -->

                                    <section>
                                        <?php the_content(); ?>
                                    </section> <!-- end article section -->
                                    
                                    <footer>
                                        
                                    </footer> <!-- end article footer -->
                                </div>
                            <?php endwhile; ?>		
                            <?php endif; ?>

                        </div>

                    </div>

                </div>

            </div>

<?php get_footer(); ?>
