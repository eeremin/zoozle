<?php get_header(); ?>
<?php 

$cat = get_category( get_query_var( 'cat' ) );
$args = array('child_of' => $cat->cat_ID);
$categories = get_categories( $args );
//echo '<pre>';
//var_dump($cat);
$tags = get_terms(array('taxonomy' => 'post_tag', 'post_type' => 'post'));
//var_dump($tags);
//echo '</pre>';
?>
		<div class="container">
			
			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix posts-page" role="main">
				
				<div class="recipes clearfix">


			<div class="products__header clearfix">
	                    <div class="products__categories">
	                        <select class="selectpicker" name='post_cats'>
	                            <option>All</option>
	                            <?php foreach ($categories as $category): ?>
	                            	<option value="<?php echo $category->slug; ?>"><?php echo $category->name; ?></option>
	                            <?php endforeach; ?>
	                        </select>
	                    </div>
	                    <div class="products__tags clearfix"  data-attr="tag">
	                    	<?php foreach ($tags as $tag): ?>
					<a class="product__tag" data-value="<?php echo $tag->slug; ?>"><?php echo $tag->name ?></a>	                    	
	                    	<?php endforeach; ?>
	                    </div>
	                </div>
		<div id="post-ajax-list">
			<div class="row">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="col-lg-4 col-sm-6 post-card">
	                        <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix post__recipe'); ?> role="article">
	                            
	                            <header class="post__header">
	                            	<div class="post__img">
	                            		<img src="<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID)) ?>" />
	                            		<h4 class="post__title" itemprop="headline"><?php the_title(); ?></h4>
	                            	</div>
	                            </header> <!-- end article header -->
	
	                            <section class="post__content-wrapper">
		                            <div class="post__content">
		                                <?php 
		                                	the_content();
		                                ?>
		                                <a href="#" class="post__content-hide-link">Hide</a>
		                            </div> <!-- end article section -->
	
		                            <a href="#" class="post__content-show-link">Show</a>
	                            </section>
	                            
	                            <footer class="post__tags clearfix">
	                            	<?php 
	                            		$posttags = get_the_tags();
	                            	    foreach ($posttags as $posttag):
	                            	?>
		                                <a href="#" class="product__tag"><?php echo $posttag->name; ?></a>
	                                <?php endforeach; ?>
	                            </footer> <!-- end article footer -->
	                        
	                        </article> <!-- end article -->
                   	 </div>
					
			<?php endwhile; ?>	
			</div>

		</div> <!-- end #post-ajax-list -->
					
					<?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>
						
						<?php wp_bootstrap_page_navi(); // use the page navi function ?>

					<?php } else { // if it is disabled, display regular wp prev & next links ?>
						<nav class="wp-prev-next">
							<ul class="pager">
								<li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
								<li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
							</ul>
						</nav>
					<?php } ?>
								
					
					<?php else : ?>
					
					<div class="container">
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("No Posts Yet", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, What you were looking for is not here.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					</div>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
			</div> <!-- end #content -->

		</div>

	</div>


<script>
ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
function refresh_page() {
	// get filter options
	var cat = $("[name='post_cats']").val();
	var tags = Array();
	$('[data-attr="tag"] a.match').each(function(key, value){
		tags.push($(value).attr('data-value'));
	});
	tags.join(',');
	console.log(cat);
	console.log(tags);
	
	jQuery.post(
	    ajax_url, 
	    {
	        'action': 'filter_post',
	        'data':   {'cat':cat, 'tags':tags}
	    }, 
	    function(response){
	        //console.log('The server responded: ' + response);
	        $('#post-ajax-list').html(response);
	    }
	);
}
function change_page(page) {
	// get filter options
	var cat = $("[name='post_cats']").val();
	var tags = Array();
	$('[data-attr="tag"] a.match').each(function(key, value){
		tags.push($(value).attr('data-value'));
	});
	tags.join(',');
	
	jQuery.post(
	    ajax_url, 
	    {
	        'action': 'filter_post',
	        'data':   {'cat':cat, 'tags':tags, 'page':page}
	    }, 
	    function(response){
	        //console.log('The server responded: ' + response);
	        $('#post-ajax-list').html(response);
	    }
	);
}
jQuery('body').on('click', '#post-ajax-list [data-page]', function(e){
	e.preventDefault();
	change_page(jQuery(this).attr('data-page'));
	return false;
});
jQuery(document).ready(function($){
	refresh_page();

	$('[data-attr="tag"]').click(function(){
		refresh_page();
	});

	$('[name="post_cats"]').change(function(){
		refresh_page();
	});
});



</script>


<?php get_footer(); ?>
