			<footer class="footer">
                <div class="container-fluid">
                    <div class="footer-content text-center">
                        <span><a href="<?= get_permalink(get_page_by_title('Terms Of Service')->ID) ?>">Terms Of Service</a>. &#169;2016 Zozzle. All Rights Reserved. <a href="mailto:hello@zozzle.com">hello@zozzle.com</a></span>
                    </div>
                </div>
			</footer> <!-- end footer -->
		
		</div> <!-- end #container -->
				
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

        <script src="http://code.jquery.com/jquery.min.js"></script>
        <script src="/wp-content/themes/zozzle/js/parallax.js-1.4.2/parallax.min.js"></script>
        <script src="/wp-content/themes/zozzle/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript">
            $('p').removeClass('lead');
            function addToCart(id, cart) {
                $.ajax("?add-to-cart=" + id, {
                    success: function() {
                        swal({
                            title:              "Product added to cart",
                            text:               "You've added a product to your cart!",
                            type:               "success",
                            showCancelButton:   true,
                            cancelButtonText:   "Continue Shopping",
                            confirmButtonText:  "Go To Cart",
                            closeOnConfirm:     true,
                            closeOnCancel:      true
                        }, function(isConfirm) {
                            if (isConfirm) {
                                window.location = cart;
                            }
                        })
                    }
                });
                console.log("Added product " + id + " to the cart");
            }


            $(function() {
              $("#header-links a[href*='#']:not([href='#'])").click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 500);
                        return false;
                    }
                }
              });
            });
        </script>
	</body>

</html>
