<?php

// support woocommerce
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}




// ********* Get all products and variations and sort alphbetically, return in array (title, sku, id)*******
function get_woocommerce_product_list() {
    $full_product_list = array();
    $args = array(
        'post_type' => 'product',
    );
    $loop = new WP_Query( $args );
    if ( $loop->have_posts() ) {
        while ( $loop->have_posts() ) : $loop->the_post();
            $theid = get_the_ID();
            $product = new WC_Product($theid);

            $attrs = get_post_meta( get_the_ID(), '_product_attributes', true);
            $attributes = array();
            foreach ($attrs as $attr) {
                $attributes[] = array(
                    'name'  => $attr['name'],
                    'value' => $attr['value'],
                );
            }

            $full_product_list[] = array(
                'id'        => $theid,
                'title'     => get_the_title($theid),
                'price'     => $product->price,
                'content'   => get_post($theid)->post_content,
                'attributes'=> $attributes,
                'product'   => $product,
            );
        endwhile;
    }    wp_reset_postdata();

    return $full_product_list;
}


// ********* Get all featured products and variations and sort alphbetically, return in array (title, sku, id)*******
function get_woocommerce_featured_product_list() {
    $featured_product_list = array();
    $args = array(
        'post_type' => 'product',
        'meta_key' => '_featured',
        'meta_value' => 'yes',
        'posts_per_page' => 4
    );
    $loop = new WP_Query( $args );
    if ( $loop->have_posts() ) {
        while ( $loop->have_posts() ) : $loop->the_post();
            $theid = get_the_ID();
            $product = new WC_Product($theid);

            $attrs = get_post_meta( get_the_ID(), '_product_attributes', true);
            $attributes = array();
            foreach ($attrs as $attr) {
                $attributes[] = array(
                    'name'  => $attr['name'],
                    'value' => $attr['value'],
                );
            }

            $featured_product_list[] = array(
                'id'        => $theid,
                'title'     => get_the_title($theid),
                'price'     => $product->price,
                'content'   => get_post($theid)->post_content,
                'attributes'=> $attributes,
                'product'   => $product,
            );
        endwhile;
    }    wp_reset_postdata();

    return $featured_product_list;
}

// Get posts by category
function getBlogPosts($category, $count) {
    $args = array(
        'posts_per_page'   => $count,
        'offset'           => 0,
        'category'         => '',
        'category_name'    => $category,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'post',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'author'       => '',
        'post_status'      => 'publish',
        'suppress_filters' => true 
    );

    $posts_array = get_posts($args);
    
    $posts = array();
    foreach ($posts_array as $post) {
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        $posts[] = array(
            'title'     => $post->post_title,
            'content'   => $post->post_content,
            'image'     => $feat_image,
        );
    }

    return $posts;
}

?>
