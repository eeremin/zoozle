<?php
/*
Template Name: custom page
*/
?>

<?php get_header(); ?>

			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix posts-page" role="main">

                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="container-fluid custom_page">
                            <header>
                                <div class="col-xs-12">
                                    <h1 class="single-title" itemprop="headline"><?php the_title(); ?></h1>
                                </div>
                                
                            </header> <!-- end article header -->

                            <section>
                                <div class="col-xs-12 col-sm-6">
                                    <?php the_content(); ?>
                                </div>
                        
                            </section> <!-- end article section -->
                            
                            <footer>
                                
                            </footer> <!-- end article footer -->
                        </div>
                    <?php endwhile; ?>		
                    <?php endif; ?>

                </div>

            </div>

<?php get_footer(); ?>
